# vhs_ingestion

Ingestion scripts for capturing VHS tapes, splitting them out into individual files based on scenes, and copying them to a remote (off machine) target for sharing and safekeeping. 